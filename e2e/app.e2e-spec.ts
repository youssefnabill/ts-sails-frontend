import { TsSailsAngularPage } from './app.po';

describe('ts-sails-angular App', function() {
  let page: TsSailsAngularPage;

  beforeEach(() => {
    page = new TsSailsAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
