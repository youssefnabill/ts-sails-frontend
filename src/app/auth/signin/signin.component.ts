import { IsSuperAdminGuard } from './../../guards/isSuperAdmin/is-super-admin.guard';
import { logging } from 'selenium-webdriver';
import { Component, OnInit } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {UserModel} from '../../models/user.model';
import {AuthService} from '../auth.service';
import {MessageService} from '../../messages/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers: [UserModel]
})
export class SigninComponent implements OnInit {

  constructor(private user: UserModel, public authService: AuthService , private messageService: MessageService, private router: Router) {
  }

  ngOnInit() {
    this.user = new UserModel();
  }

  signin(email: string, password: string, userType: string) {
    if (!password || !email) {
      this.messageService.addError('Missing email or password field');
      return;
    }
    this.user.email = email;
    this.user.password = password;
    this.authService.signin(this.user, userType).subscribe(res => {
      localStorage.setItem('token', res.token);
      localStorage.setItem('isAdmin', res.isAdmin);
      localStorage.setItem('isSuper', res.isSuper);
      localStorage.setItem('id', res.id);
      this.authService.LoggedIn = true;
      this.authService.Admin = res.isAdmin;
      this.authService.Super = res.isSuper;
      if (res.isAdmin) {
        this.router.navigate(['/trx/all']);
      }else {
        this.router.navigate(['/trx']);
      }
    }, err => {
      console.log(err);
      if (err.status === 500) {
        this.messageService.addError('Wrong email or password');
        return;
      }
      this.messageService.addError('Something went wrong Please try again');
    });
  }

}




