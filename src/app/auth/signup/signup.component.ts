import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../models/user.model';
import { AuthService } from '../auth.service';
import {MessageService} from '../../messages/message.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [UserModel, AuthService]
})
export class SignupComponent implements OnInit {

  constructor(private user: UserModel, private authService: AuthService, public messageService: MessageService, private router: Router) {
  }

  ngOnInit() {
    this.user = new UserModel();
  }

  signup(name: string, username: string, phoneNo: string, email: string, cmail: string, password: string) {
    if (!(email === cmail)) {
      this.messageService.addError('email doesn\'t match');
      return;
    }
    if (!name || !username || !phoneNo || !email || !cmail || !password) {
      this.messageService.addError('All feilds are required');
      return;
    }
    this.user.init(null, name, username, email, password, phoneNo, 0, 1, 1, 0, 0, 0, 0);
    const res = this.authService.signup(this.user).subscribe(data => {
      console.log(data);
      this.messageService.addMsg('user created successfully');
      this.router.navigate(['/signin']);
    }, error => {
      console.log(error);
      this.messageService.addError('error while creating user' + error);
    });
  }

}
