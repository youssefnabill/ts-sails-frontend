import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { logging } from 'selenium-webdriver';
@Injectable()
export class AuthService {
  public LoggedIn: boolean;
  public Admin: boolean;
  public Super: boolean;
  constructor(private user: UserModel, private httpClient: HttpClient) { }

  signin(user, userType) {
    const body = {
      email: user.email,
      password: user.password
    };
    return this.httpClient.post('http://localhost:1337/' + userType + '/login', body)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }
  signup(user): Observable<any> {
    const body = {
      name: user.name,
      email: user.email,
      password: user.password,
      phone_number : user.phone_number,
      username: user.username,
      limit_id: '1',
      fees_id: '1',
      balance: '0'
    };
    return this.httpClient.post('http://localhost:1337/user/create', body)
      .catch((error: any) => Observable.throw(error || 'Server error'));
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('isAdmin');
    localStorage.removeItem('isSuper');
    localStorage.removeItem('id');
    this.LoggedIn = false;
  }

  isLoggedIn(): boolean{
    if (!localStorage.getItem('token')){
      return false;
    }
    return true;

  }

  isAdmin(): boolean {
    if (!localStorage.getItem('isAdmin') || localStorage.getItem('isAdmin') === 'false'){
      return false;
    }
    return true;
  }

  isSuperAdmin(): boolean {
    if (!localStorage.getItem('isSuper') || localStorage.getItem('isSuper') === 'false') {
      return false;
    }
    return true;
  }
}
