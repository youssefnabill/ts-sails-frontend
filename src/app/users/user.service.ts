import { UserModel } from './../models/user.model';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserService {

  constructor(private user: UserModel, private httpClient: HttpClient) { }
  getUsers() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return this.httpClient.get('http://localhost:1337/user/getall', httpOptions)
      .catch((error: any) => Observable.throw(error.error || 'Server error'));
  }

  async getUserById(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/user/get/' + id, httpOptions).toPromise();
  }

  async deleteUser(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.delete('http://localhost:1337/user/delete/' + id, httpOptions).toPromise();
  }

  async updateUser(id: number, userRow: UserModel) {
    const body = {
      name : userRow.name,
      username : userRow.username,
      email : userRow.email,
      phone_number : userRow.phone_number,
      fees_id : userRow.fees_id,
      limit_id : userRow.limit_id
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.put('http://localhost:1337/user/update/' + id, body, httpOptions).toPromise();
  }

  async getUserByEmail(email: string) {
    const body = {
      email : email
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.post('http://localhost:1337/user/find/email', body, httpOptions).toPromise();

  }

}
