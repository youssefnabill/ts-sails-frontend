import { Component, OnInit } from '@angular/core';
import {UserModel} from '../models/user.model';
import {MessageService} from '../messages/message.service';
import {UserService} from './user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserModel, UserService]
})
export class UsersComponent implements OnInit {
  users: UserModel[];

  constructor(private user: UserModel, private userService: UserService, private messageService: MessageService) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(users => {
      this.users = users;
      console.log(users);
    }, err => {
      console.log(err);
    });
  }

}
