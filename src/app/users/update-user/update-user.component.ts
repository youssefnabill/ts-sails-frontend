import { Route, Router, ActivatedRoute } from '@angular/router';
import { MessageService } from './../../messages/message.service';
import { UserModel } from './../../models/user.model';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css'],
  providers: [UserService, UserModel]
})
export class UpdateUserComponent implements OnInit {
  userId: number;
  user: any;

  constructor(private messageService: MessageService, private userService: UserService,
              private router: Router, private activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    this.user = new UserModel();
    this.userId = this.activatedRoute.snapshot.params.id;
    try {
      this.user = await this.userService.getUserById(this.userId);
      console.log(this.user);
    } catch (error) {
      this.messageService.addError(error);
    }
  }

  async updateUser(id: number, name: string, username: string, email: string, phone_number: string, fees_id: number, limit_id: number) {
    this.user.email = email;
    this.user.name = name;
    this.user.username = username;
    this.user.phone_number = phone_number;
    this.user.fees_id = fees_id;
    this.user.limit_id = limit_id;

    try {
      const updatedAdminRow = await this.userService.updateUser(id, this.user);
      this.messageService.addMsg('User Account updated successfully');
        this.router.navigate(['/users']);
    } catch (error) {
      this.messageService.addError(error.message);
        console.log(error.error);
    }

  }



}
