import { MessageService } from './../../messages/message.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../user.service';
import {UserModel} from '../../models/user.model';

@Component({
  selector: 'app-single-user',
  templateUrl: './single-user.component.html',
  styleUrls: ['./single-user.component.css'],
  providers: [UserService, UserModel]
})
export class SingleUserComponent implements OnInit {
  userId: number;
  user: any;
  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private messageService: MessageService) { }

  async ngOnInit() {
    this.user = new UserModel();
    this.userId = this.activatedRoute.snapshot.params.id;
    try {
      this.user = await this.userService.getUserById(this.userId);
    } catch (error) {
      this.messageService.addError(error);
    }

  }

  async deleteUser(id: number) {
    try {
      await this.userService.deleteUser(id);
    } catch (error) {
      this.messageService.addError(error);
    }
  }

}
