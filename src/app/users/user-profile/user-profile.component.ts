import { MessageService } from './../../messages/message.service';
import { UserService } from './../user.service';
import { UserModel } from './../../models/user.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [UserModel, UserService]
})
export class UserProfileComponent implements OnInit {

  user: UserModel;
  userId: number;

  constructor(private userService: UserService, private messageService: MessageService) { }

  async ngOnInit() {
    try {
      this.userId = parseInt(localStorage.getItem('id'), 10);
      const user: any = await this.userService.getUserById(this.userId);
      this.user = user;
    } catch (error) {
      this.messageService.addError(error);
    }
  }

}
