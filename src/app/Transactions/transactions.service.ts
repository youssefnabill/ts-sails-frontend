import { TransactionModel } from './../models/transaction.model';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class TransactionsService {

  constructor(private transaction: TransactionModel,  private httpClient: HttpClient) { }

  async getUserTransactions(userId) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/trx/user/' + userId, httpOptions).toPromise();
  }

  async makeTransaction(sender_id: number, receiver_id: number, amount: number, senderType: string, receiverType: string) {

    const body = {
      sender_id : sender_id,
      receiver_id : receiver_id,
      amount : amount,
      senderType: senderType,
      receiverType: receiverType
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.post('http://localhost:1337/trx/make/', body, httpOptions).toPromise();
  }

  async getTransactionById(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/trx/get/' + id, httpOptions).toPromise();
  }

  async allTransactions() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/trx/getall', httpOptions).toPromise();
  }
}
