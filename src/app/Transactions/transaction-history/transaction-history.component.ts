import { ActivatedRoute } from '@angular/router';
import { MessageService } from './../../messages/message.service';
import { TransactionsService } from './../transactions.service';
import { TransactionModel } from './../../models/transaction.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.css'],
  providers: [TransactionModel, TransactionsService]
})
export class TransactionHistoryComponent implements OnInit {

  userId: number;
  transactions: TransactionModel[];

  constructor(private transactionsService: TransactionsService,
              private messageService: MessageService, private activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    try {
      this.userId = parseInt(localStorage.getItem('id'), 10);
      const transactions: any = await this.transactionsService.getUserTransactions(this.userId);
      this.transactions = transactions;
      console.log(transactions);
    } catch (error) {
      console.log(error.toString());
      this.messageService.addError(error.toString());
    }
  }

}
