import { Router } from '@angular/router';
import { UserService } from './../../users/user.service';
import { MessageService } from './../../messages/message.service';
import { TransactionModel } from './../../models/transaction.model';
import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../transactions.service';

@Component({
  selector: 'app-make-transaction',
  templateUrl: './make-transaction.component.html',
  styleUrls: ['./make-transaction.component.css'],
  providers: [TransactionsService, TransactionModel, UserService]
})
export class MakeTransactionComponent implements OnInit {

  constructor(private transactionService: TransactionsService, private messageService: MessageService,
              private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  async makeTransaction(receiverEmail: string, amount: number) {
    try {
      const receiverUser: any = await this.userService.getUserByEmail(receiverEmail);
      const senderId: number = parseInt(localStorage.getItem('id'), 10);
      const senderUser: any = await this.userService.getUserById(senderId);
      await this.transactionService.makeTransaction(senderId, receiverUser.id, amount, senderUser.type, receiverUser.type);
      this.messageService.addMsg('transaction completed');
      this.router.navigate(['/trx']);
    } catch (error) {
      this.messageService.addError(error.error);
      console.log(error.error);

    }
  }

}
