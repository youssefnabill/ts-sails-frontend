import { MessageService } from './../../messages/message.service';
import { TransactionsService } from './../transactions.service';
import { Component, OnInit } from '@angular/core';
import { TransactionModel } from '../../models/transaction.model';

@Component({
  selector: 'app-all-transactions',
  templateUrl: './all-transactions.component.html',
  styleUrls: ['./all-transactions.component.css'],
  providers: [TransactionsService, TransactionModel]
})
export class AllTransactionsComponent implements OnInit {

  transactions: TransactionModel[];

  constructor(private trx: TransactionModel, private transactionService: TransactionsService, private messageService: MessageService) { }

  async ngOnInit() {
    try {
      const trxs: any = await this.transactionService.allTransactions();
      this.transactions = trxs;
      console.log(this.transactions);
  } catch (error) {
      this.messageService.addError(error);
  }
  }

}
