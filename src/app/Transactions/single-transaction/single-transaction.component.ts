import { MessageService } from './../../messages/message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TransactionModel } from './../../models/transaction.model';
import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../transactions.service';

@Component({
  selector: 'app-single-transaction',
  templateUrl: './single-transaction.component.html',
  styleUrls: ['./single-transaction.component.css'],
  providers: [TransactionModel, TransactionsService]
})
export class SingleTransactionComponent implements OnInit {

  trxId: number;
  trx: TransactionModel;
  constructor(private activatedRoute: ActivatedRoute, private transactionService: TransactionsService,
              private messageService: MessageService, private router: Router) { }

  async ngOnInit() {
    this.trx = new TransactionModel();
    this.trxId = this.activatedRoute.snapshot.params.id;
    try {
      const selectedTrx: any = await this.transactionService.getTransactionById(this.trxId);
      this.trx = selectedTrx;
    }catch (e) {
      this.messageService.addError(e);
    }
  }

}
