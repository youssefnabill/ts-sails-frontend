import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
  messages: string[] = [];
  errors: string[] = [];

  addMsg(message: string) {
    this.clear();
    this.messages.push(message);
    console.log(this.messages);
  }
  addError(error: string) {
    this.clear();
    this.errors.push(error);
    console.log(this.errors);
  }

  clear() {
    this.messages = [];
    this.errors = [];
  }

}
