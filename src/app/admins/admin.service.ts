import { AdminModel } from './../models/admin.model';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AdminService {

  constructor(private adminModel: AdminModel, private httpClient: HttpClient) { }

  async getAdmins() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/admin/getall', httpOptions).toPromise();
  }

  async getAdminById(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/admin/get/' + id, httpOptions).toPromise();
  }

  async createNewAdmin(adminModel: AdminModel) {
    const admin = {
      email: adminModel.email,
      username: adminModel.username,
      name: adminModel.name,
      password: adminModel.password
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.post('http://localhost:1337/admin/create', admin, httpOptions).toPromise();
  }

  async deleteAdmin(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.delete('http://localhost:1337/admin/delete/' + id, httpOptions).toPromise();
  }

  async updateAdmin(id: number, adminRow: AdminModel) {
    const body = {
      name : adminRow.name,
      username : adminRow.username,
      email : adminRow.email
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.put('http://localhost:1337/admin/update/' + id, body, httpOptions).toPromise();
  }
}
