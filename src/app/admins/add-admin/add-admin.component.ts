import { MessageService } from './../../messages/message.service';
import { AdminService } from './../admin.service';
import { AdminModel } from './../../models/admin.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.css']
})
export class AddAdminComponent implements OnInit {

  constructor(private adminModel: AdminModel, private adminService: AdminService,
              private messageService: MessageService, private router: Router) { }

  ngOnInit() {
    this.adminModel = new AdminModel();
  }

  async createAdmin(name: string, username: string, email: string, cmail: string, password: string) {
    if (!(email = cmail)) {
      this.messageService.addError('Email doesn\'t match');
      return false;
    }
    this.adminModel.email = email;
    this.adminModel.name = name;
    this.adminModel.username = name;
    this.adminModel.password = password;
    console.log(this.adminModel);
    try {
      const newAdmin = await this.adminService.createNewAdmin(this.adminModel);
      this.messageService.addMsg('new admin is created successfully');
      this.router.navigate(['/admins']);
    } catch (error) {
      this.messageService.addError(error.error.message);
      console.log(error.error);
    }

  }

}
