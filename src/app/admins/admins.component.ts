import { AdminService } from './admin.service';
import { MessageService } from './../messages/message.service';
import { AdminModel } from './../models/admin.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css'],
  providers: [AdminService, AdminModel]
})
export class AdminsComponent implements OnInit {

  admins: AdminModel[];
  constructor(private admin: AdminModel, private adminService: AdminService, private messageService: MessageService) { }

  async ngOnInit() {
    try {
        const admins: any = await this.adminService.getAdmins();
        this.admins = admins;
    } catch (error) {
        this.messageService.addError(error.toString());
    }
  }

}
