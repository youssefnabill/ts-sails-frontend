import { AdminService } from './../admin.service';
import { MessageService } from './../../messages/message.service';
import { AdminModel } from './../../models/admin.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-admin',
  templateUrl: './update-admin.component.html',
  styleUrls: ['./update-admin.component.css'],
  providers: [AdminService, AdminModel]
})
export class UpdateAdminComponent implements OnInit {

  adminId: number;
  admin: any;
  constructor(private messageService: MessageService, private adminService: AdminService,
              private router: Router, private activatedRoute: ActivatedRoute ) { }

  async ngOnInit() {
    this.admin = new AdminModel();
    this.adminId = this.activatedRoute.snapshot.params.id;
    try {
      this.admin = await this.adminService.getAdminById(this.adminId);
    } catch (error) {
      this.messageService.addError(error);
    }
  }

  async updateAdmin(id: number, name: string, username: string, email: string) {
    this.admin.email = email;
    this.admin.name = name;
    this.admin.username = username;

    try {
      const updatedAdminRow = await this.adminService.updateAdmin(id, this.admin);
      this.messageService.addMsg('Admin Account updated successfully');
        this.router.navigate(['/admins']);
    } catch (error) {
      this.messageService.addError(error.message);
        console.log(error.error);
    }

  }


}
