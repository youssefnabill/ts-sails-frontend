import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from './../admin.service';
import { Component, OnInit } from '@angular/core';
import { AdminModel } from '../../models/admin.model';
import { MessageService } from '../../messages/message.service';

@Component({
  selector: 'app-single-admin',
  templateUrl: './single-admin.component.html',
  styleUrls: ['./single-admin.component.css'],
  providers: [AdminModel, AdminService]
})
export class SingleAdminComponent implements OnInit {
  adminId: number;
  adminModel: AdminModel;
  constructor(private activatedRoute: ActivatedRoute, private adminService: AdminService,
               private messageService: MessageService, private router: Router) { }

  async ngOnInit() {
    this.adminModel = new AdminModel();
    this.adminId = this.activatedRoute.snapshot.params.id;
    try {
      const admin: any = await this.adminService.getAdminById(this.adminId);
      this.adminModel = admin;
    }catch (e) {
      this.messageService.addError(e);
    }
  }

  async deleteAdmin(id: number) {
    try {
      const fee: any = await this.adminService.deleteAdmin(id);
      this.messageService.addMsg('Admin account deleted successfully');
      this.router.navigate(['admins']);
    }catch (e) {
      console.log(e);
      this.messageService.addError(e);
    }
  }

}
