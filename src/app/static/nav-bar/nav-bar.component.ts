import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
  providers: []
})
export class NavBarComponent implements OnInit {

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.initStatus();
  }

  signOut() {
    this.authService.logout();
    this.router.navigate(['/']);
    this.authService.LoggedIn = false;
    this.authService.Admin = false;
    this.authService.Super = false;
  }

  initStatus() {
    this.authService.LoggedIn = !!localStorage.getItem('token');
    this.authService.Admin = !!localStorage.getItem('isAdmin');
    this.authService.Super = !!localStorage.getItem('isSuper');
  }

}
