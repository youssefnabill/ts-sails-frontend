import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLimitsComponent } from './update-limits.component';

describe('UpdateLimitsComponent', () => {
  let component: UpdateLimitsComponent;
  let fixture: ComponentFixture<UpdateLimitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateLimitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLimitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
