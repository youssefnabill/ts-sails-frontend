import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from './../../../messages/message.service';
import { ProfilesService } from './../../profiles.service';
import { LimitModel } from './../../../models/limit.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-limits',
  templateUrl: './update-limits.component.html',
  styleUrls: ['./update-limits.component.css'],
  providers: [ProfilesService, LimitModel]
})
export class UpdateLimitsComponent implements OnInit {
  limitId: number;
  limit: any;
  constructor(private profilesService: ProfilesService, private messageService: MessageService,
               private router: Router, private activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    this.limitId = this.activatedRoute.snapshot.params.id;
    try {
      this.limit = await this.profilesService.getLimitById(this.limitId);
    } catch (error) {
      this.messageService.addError(error);
    }

  }

  async updateLimits(id: number,
                      profile_name: string,
                      daily_send_amount: any,
                      daily_receive_amount: any,
                      daily_send_count: any,
                      daily_receive_count: any) {
    this.limit.profile_name = profile_name;
    this.limit.daily_send_amount = daily_send_amount;
    this.limit.daily_receive_amount = daily_receive_amount;
    this.limit.daily_send_count = daily_send_count;
    this.limit.daily_receive_count = daily_receive_count;
    try {
      const updatedLimitRow = await this.profilesService.updateLimit(id, this.limit);
      this.messageService.addMsg('Limit profile updated successfully');
        this.router.navigate(['/limits']);
    } catch (error) {
      this.messageService.addError(error.message);
        console.log(error.error);
    }

  }

}
