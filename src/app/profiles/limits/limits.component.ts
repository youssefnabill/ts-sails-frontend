import { Component, OnInit } from '@angular/core';
import {FeesModel} from '../../models/fees.model';
import {ProfilesService} from '../profiles.service';
import {LimitModel} from '../../models/limit.model';
import {MessageService} from '../../messages/message.service';

@Component({
  selector: 'app-limits',
  templateUrl: './limits.component.html',
  styleUrls: ['./limits.component.css'],
  providers: [FeesModel, ProfilesService, LimitModel]
})
export class LimitsComponent implements OnInit {
  limits: LimitModel[];

  constructor(private limitModel: LimitModel, private profilesService: ProfilesService, private messageService: MessageService) { }

  async ngOnInit() {
    try {
      const limits: any = await this.profilesService.getLimits();
      console.log(limits);
      this.limits = limits;
    }catch (e) {
      console.log(e.toString());
      this.messageService.addError(e.toString());
    }

  }

}
