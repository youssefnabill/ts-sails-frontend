import { Router } from '@angular/router';
import { MessageService } from './../../../messages/message.service';
import { ProfilesService } from './../../profiles.service';
import { LimitModel } from './../../../models/limit.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-limit',
  templateUrl: './add-limit.component.html',
  styleUrls: ['./add-limit.component.css']
})
export class AddLimitComponent implements OnInit {

  constructor(private limitModel: LimitModel, private profileService: ProfilesService,
               private messageService: MessageService,  private router: Router) { }

  ngOnInit() {
    this.limitModel = new LimitModel();
  }

  async createLmit(profile_name: string,
              daily_send_amount: any,
              daily_receive_amount: any,
              daily_send_count: any,
              daily_receive_count: any) {
      this.limitModel.profile_name = profile_name;
      this.limitModel.daily_send_amount = daily_send_amount;
      this.limitModel.daily_receive_amount = daily_receive_amount;
      this.limitModel.daily_send_count = daily_send_count;
      this.limitModel.daily_receive_count = daily_receive_count;

      try {
        const newLimit = await this.profileService.createNewLimit(this.limitModel);
        this.messageService.addMsg('new Limit is created successfully');
        this.router.navigate(['/limits']);
      } catch (error) {
        this.messageService.addError(error.message);
        console.log(error.error);
      }
  }

}
