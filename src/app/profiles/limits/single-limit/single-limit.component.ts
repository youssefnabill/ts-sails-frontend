import { MessageService } from './../../../messages/message.service';
import { Component, OnInit } from '@angular/core';
import {LimitModel} from '../../../models/limit.model';
import {FeesModel} from '../../../models/fees.model';
import {ProfilesService} from '../../profiles.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-single-limit',
  templateUrl: './single-limit.component.html',
  styleUrls: ['./single-limit.component.css'],
  providers: [ProfilesService, FeesModel, LimitModel]
})
export class SingleLimitComponent implements OnInit {
  limitId: number;
  limitModel: LimitModel;

  constructor(private activatedRoute: ActivatedRoute, private profileService: ProfilesService,
              private messageService: MessageService, private router: Router) { }

  async ngOnInit() {
    this.limitModel = new LimitModel();
    this.limitId = this.activatedRoute.snapshot.params.id;
    try {
      const limit: any = await this.profileService.getLimitById(this.limitId);
      this.limitModel = limit;
    }catch (e) {
      console.log(e.toString());
    }
  }

  async deleteLimit(id: number) {
    try {
      const limit: any = await this.profileService.deleteLimit(id);
      this.messageService.addMsg('profile deleted successfully');
      this.router.navigate(['limits']);
    }catch (e) {
      console.log(e);
      this.messageService.addError(e);
    }
  }

}
