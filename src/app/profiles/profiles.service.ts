import { LimitModel } from './../models/limit.model';
import { Injectable } from '@angular/core';
import {FeesModel} from '../models/fees.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProfilesService {

  constructor(private feesModel: FeesModel, private limitModel: LimitModel, private httpClient: HttpClient) { }

  // fees methods
  async getFees() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/fees/getall', httpOptions).toPromise();
  }

  async getFeesById(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/fees/get/' + id, httpOptions).toPromise();
  }

  async createNewFee(fee: FeesModel) {
    const newFee = {
      profile_name : fee.profile_name,
      sender_fees: fee.sender_fees,
      receive_fees: fee.receive_fees,
      s_fees_type: fee.s_fees_type,
      r_fees_type: fee.r_fees_type
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.post('http://localhost:1337/fees/create', newFee, httpOptions).toPromise();
  }

  async deleteFee(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.delete('http://localhost:1337/fees/delete/' + id, httpOptions).toPromise();
  }

  async updateFee(id: number, feeRow: FeesModel) {
    const body = {
      profile_name: feeRow.profile_name,
      sender_fees: feeRow.sender_fees,
      receive_fees: feeRow.receive_fees,
      s_fees_type: feeRow.s_fees_type,
      r_fees_type: feeRow.r_fees_type
    }
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.put('http://localhost:1337/fees/update/' + id, body, httpOptions).toPromise();
  }

  // limits methods
  async getLimits() {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/limit/getall', httpOptions).toPromise();
  }

  async getLimitById(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.get('http://localhost:1337/limit/get/' + id, httpOptions).toPromise();
  }

  async createNewLimit(limit: LimitModel) {
    const newLimit = {
      profile_name : limit.profile_name,
      daily_send_amount: limit.daily_send_amount,
      daily_receive_amount: limit.daily_receive_amount,
      daily_send_count: limit.daily_send_count,
      daily_receive_count: limit.daily_receive_count
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.post('http://localhost:1337/limit/create', newLimit, httpOptions).toPromise();
  }

  async deleteLimit(id: number) {
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.delete('http://localhost:1337/limit/delete/' + id, httpOptions).toPromise();
  }

  async updateLimit(id: number, limitRow: LimitModel) {
    const body = {
      profile_name: limitRow.profile_name,
      daily_send_amount: limitRow.daily_send_amount,
      daily_receive_amount: limitRow.daily_receive_amount,
      daily_send_count: limitRow.daily_send_count,
      daily_receive_count: limitRow.daily_receive_count
    };
    const token = localStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': token
      })
    };
    return await this.httpClient.put('http://localhost:1337/limit/update/' + id, body, httpOptions).toPromise();
  }

}
