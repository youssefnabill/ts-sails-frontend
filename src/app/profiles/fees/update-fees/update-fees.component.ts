import { MessageService } from './../../../messages/message.service';
import { ProfilesService } from './../../profiles.service';
import { Component, OnInit } from '@angular/core';
import { FeesModel } from '../../../models/fees.model';
import { Route, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-fees',
  templateUrl: './update-fees.component.html',
  styleUrls: ['./update-fees.component.css'],
  providers: [ProfilesService, FeesModel]
})
export class UpdateFeesComponent implements OnInit {
  feeId: number;
  fee: any;

  constructor(private profilesService: ProfilesService, private messageService: MessageService,
              private router: Router, private activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    this.feeId = this.activatedRoute.snapshot.params.id;
    try {
      this.fee = await this.profilesService.getFeesById(this.feeId);
    } catch (error) {
      this.messageService.addError(error);
    }
  }

  async updateFees(id: number,
                    profile_name: string,
                    sender_fees: string,
                    receive_fees: string,
                    s_fee_type: string,
                    r_fee_type: string) {
    this.fee.profile_name = profile_name;
    this.fee.sender_fees = parseInt(sender_fees, 10);
    this.fee.receive_fees = parseInt(receive_fees, 10);
    this.fee.s_fees_type = s_fee_type;
    this.fee.r_fees_type = r_fee_type;
    try {
      const updatedFeeRow = await this.profilesService.updateFee(id, this.fee);
      this.messageService.addMsg('Fees profile updated successfully');
        this.router.navigate(['/fees']);
    } catch (error) {
      this.messageService.addError(error.message);
        console.log(error.error);
    }
  }

}
