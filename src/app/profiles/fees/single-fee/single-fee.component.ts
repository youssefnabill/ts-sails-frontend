import { MessageService } from './../../../messages/message.service';
import { Component, OnInit } from '@angular/core';
import {ProfilesService} from '../../profiles.service';
import {FeesModel} from '../../../models/fees.model';
import { ActivatedRoute, Router } from '@angular/router';
import {LimitModel} from '../../../models/limit.model';

@Component({
  selector: 'app-single-fee',
  templateUrl: './single-fee.component.html',
  styleUrls: ['./single-fee.component.css'],
  providers: [ProfilesService, FeesModel, LimitModel]
})
export class SingleFeeComponent implements OnInit {
  feeId: number;
  feeModel: FeesModel;
  constructor(private activatedRoute: ActivatedRoute, private profileService: ProfilesService,
               private messageService: MessageService, private router: Router) { }

  async ngOnInit() {
    this.feeModel = new FeesModel();
    this.feeId = this.activatedRoute.snapshot.params.id;
    try {
      const fee: any = await this.profileService.getFeesById(this.feeId);
      this.feeModel = fee;
    }catch (e) {
      console.log(e.toString());
    }
  }

  async deleteFee(id: number) {
    try {
      const fee: any = await this.profileService.deleteFee(id);
      this.messageService.addMsg('profile deleted successfully');
      this.router.navigate(['fees']);
    }catch (e) {
      console.log(e);
      this.messageService.addError(e);
    }
  }

}
