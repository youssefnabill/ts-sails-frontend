import { FeesModel } from './../../../models/fees.model';
import { Router } from '@angular/router';
import { MessageService } from './../../../messages/message.service';
import { ProfilesService } from './../../profiles.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-fee',
  templateUrl: './add-fee.component.html',
  styleUrls: ['./add-fee.component.css']
})
export class AddFeeComponent implements OnInit {

  constructor(private feeModel: FeesModel, private profileService: ProfilesService,
    private messageService: MessageService,  private router: Router) { }

  ngOnInit() {
    this.feeModel = new FeesModel();
  }

  async createFee( profile_name: string,
              sender_fees: string,
              receive_fees: string,
              s_fee_type: string,
              r_fee_type: string) {
      this.feeModel.profile_name = profile_name;
      this.feeModel.sender_fees = parseInt(sender_fees, 10);
      this.feeModel.receive_fees = parseInt(receive_fees, 10);
      this.feeModel.s_fees_type = s_fee_type;
      this.feeModel.r_fees_type = r_fee_type;
      try {
        const newFee = await this.profileService.createNewFee(this.feeModel);
        this.messageService.addMsg('new Fees profile is created successfully');
        this.router.navigate(['/fees']);
      } catch (error) {
        this.messageService.addError(error.error.message);
        console.log(error.error);
      }
  }

}
