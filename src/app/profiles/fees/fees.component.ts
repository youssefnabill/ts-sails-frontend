import { Component, OnInit } from '@angular/core';
import {FeesModel} from '../../models/fees.model';
import {ProfilesService} from '../profiles.service';
import {MessageService} from '../../messages/message.service';
import {LimitModel} from '../../models/limit.model';

@Component({
  selector: 'app-fees',
  templateUrl: './fees.component.html',
  styleUrls: ['./fees.component.css'],
  providers: [FeesModel, LimitModel, ProfilesService]
})
export class FeesComponent implements OnInit {
  fees: FeesModel[];

  constructor(private feesModel: FeesModel, private profilesService: ProfilesService, private messageService: MessageService) { }

  async ngOnInit() {
    try {
      const fees: any = await this.profilesService.getFees();
      console.log(fees);
      this.fees = fees;
    }catch (e) {
      console.log(e.toString());
      this.messageService.addError(e.toString());
    }

  }

}
