import { Injectable } from '@angular/core';

@Injectable() // class annotation as Injectable
export class AdminModel {
  id: number = null;
  name: string = null;
  username: string = null;
  email: string = null;
  password: string = null;
  is_super: string = null;

}

