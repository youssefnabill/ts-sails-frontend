import { Injectable } from '@angular/core';

@Injectable() // class annotation as Injectable
export class UserModel {
  id: number = null;
  name: string = null;
  username: string = null;
  email: string = null;
  password: string = null;
  phone_number: string = null;
  balance: number = null;
  limit_id: number = null;
  fees_id: number = null;
  day_sent: number = null;
  day_received: number = null;
  month_sent: number = null;
  month_receive: number = null;

  // // methods
  constructor() {}
  init(id: number, name: string, username: string, email: string, password: string,
       phone_number: string, balance: number, limit_id: number,
       fees_id: number, day_sent: number, day_received: number,
       month_sent: number, month_receive: number): void {
      this.id = id;
      this.name = name;
      this.username = username;
      this.email = email;
      this.password = password;
      this.phone_number = phone_number;
      this.balance = balance;
      this.limit_id = limit_id;
      this.fees_id = fees_id;
      this.day_sent = day_sent;
      this.day_received = day_received;
      this.month_sent = month_sent;
      this.month_receive = month_receive;
  }
}

