import { Injectable } from '@angular/core';

@Injectable() // class annotation as Injectable

export class LimitModel {
  id: number = null;
  profile_name: string;
  daily_send_amount: number;
  daily_receive_amount: number;
  daily_send_count: number;
  daily_receive_count: number;

  // methods
  constructor() {}
  init(id: number, profile_name: string, daily_send_amount: number, daily_receive_amount: number,
       daily_send_count: number, daily_receive_count: number) {
    this.id = id;
    this.profile_name = profile_name;
    this.daily_send_amount = daily_send_amount;
    this.daily_receive_amount = daily_receive_amount;
    this.daily_send_count = daily_send_count;
    this.daily_receive_count = daily_receive_count;
  }
}
