import { Injectable } from '@angular/core';

@Injectable() // class annotation as Injectable
export class TransactionModel {
  id: number;
  amount: number;
  sender_id: number;
  receiver_id: number;
  sender_balance_before: number;
  sender_balance_after: number;
  receiver_balance_before: number;
  receiver_balance_after: number;
  sender_fees: number;
  receiver_fees: number;
  createdAt: Date;
  date: Date;
}
