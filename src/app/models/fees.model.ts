import { Injectable } from '@angular/core';

@Injectable() // class annotation as Injectable

export class FeesModel {
  id: number = null;
  profile_name: string = null;
  sender_fees: number = null;
  s_fees_type: string = null;
  receive_fees: number = null;
  r_fees_type: string = null;

  // methods
  constructor() {}
  init(id: number, profile_name: string, sender_fees: number, s_fees_type: string, receive_fees: number, r_fees_type: string){
    this.id = id;
    this.profile_name = profile_name;
    this.sender_fees = sender_fees;
    this.s_fees_type = s_fees_type;
    this.receive_fees = receive_fees;
    this.r_fees_type = r_fees_type;
  }
}
