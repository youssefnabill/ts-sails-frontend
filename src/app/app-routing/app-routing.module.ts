import { UserProfileComponent } from './../users/user-profile/user-profile.component';
import { AllTransactionsComponent } from './../Transactions/all-transactions/all-transactions.component';
import { MakeTransactionComponent } from './../Transactions/make-transaction/make-transaction.component';
import { SingleTransactionComponent } from './../Transactions/single-transaction/single-transaction.component';
import { UpdateUserComponent } from './../users/update-user/update-user.component';
import { UpdateAdminComponent } from './../admins/update-admin/update-admin.component';
import { FeesModel } from './../models/fees.model';
import { LimitModel } from './../models/limit.model';
import { AdminService } from './../admins/admin.service';
import { AdminModel } from './../models/admin.model';
import { AdminsComponent } from './../admins/admins.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SigninComponent } from '../auth/signin/signin.component';
import { SignupComponent } from '../auth/signup/signup.component';
import { UsersComponent} from '../users/users.component';
import { SingleUserComponent} from '../users/single-user/single-user.component';
import { FeesComponent} from '../profiles/fees/fees.component';
import { LimitsComponent} from '../profiles/limits/limits.component';
import { SingleFeeComponent} from '../profiles/fees/single-fee/single-fee.component';
import { SingleLimitComponent} from '../profiles/limits/single-limit/single-limit.component';
import { TransactionHistoryComponent} from '../Transactions/transaction-history/transaction-history.component';
import { HomeComponent} from '../home/home.component';
import { IsLoggedInGuard } from '../guards/isLoggedIn/is-logged-in.guard';
import { AuthService } from '../auth/auth.service';
import { IsAdminGuard } from '../guards/isAdmin/is-admin.guard';
import { IsSuperAdminGuard } from '../guards/isSuperAdmin/is-super-admin.guard';
import { UserModel } from '../models/user.model';
import { AddAdminComponent } from '../admins/add-admin/add-admin.component';
import { SingleAdminComponent } from '../admins/single-admin/single-admin.component';
import { AddFeeComponent } from '../profiles/fees/add-fee/add-fee.component';
import { AddLimitComponent } from '../profiles/limits/add-limit/add-limit.component';
import { ProfilesService } from '../profiles/profiles.service';
import { UpdateFeesComponent } from '../profiles/fees/update-fees/update-fees.component';
import { UpdateLimitsComponent } from '../profiles/limits/update-limits/update-limits.component';

const routes: any = [

    { path: '', component: HomeComponent},
    { path: 'trx', component: TransactionHistoryComponent, canActivate: [IsLoggedInGuard] },
    { path: 'trx/get/:id', component: SingleTransactionComponent, canActivate: [IsLoggedInGuard] },
    { path: 'trx/make', component: MakeTransactionComponent, canActivate: [IsLoggedInGuard] },
    { path: 'trx/all', component: AllTransactionsComponent, canActivate: [IsAdminGuard] },


    { path: 'signin', component: SigninComponent },
    { path: 'signup', component: SignupComponent },

    { path: 'users', component: UsersComponent, canActivate: [IsLoggedInGuard, IsAdminGuard] },
    { path: 'user/get/:id', component: SingleUserComponent, canActivate: [IsLoggedInGuard]  },
    { path: 'user/update/:id', component: UpdateUserComponent, canActivate: [IsLoggedInGuard]  },
    { path: 'profile', component: UserProfileComponent, canActivate: [IsLoggedInGuard]  },

    { path: 'admins', component: AdminsComponent, canActivate: [IsLoggedInGuard, IsSuperAdminGuard] },
    { path: 'admin/get/:id', component: SingleAdminComponent, canActivate: [IsLoggedInGuard, IsSuperAdminGuard] },
    { path: 'admin/update/:id', component: UpdateAdminComponent, canActivate: [IsLoggedInGuard, IsSuperAdminGuard] },
    { path: 'admin/add', component: AddAdminComponent, canActivate: [IsLoggedInGuard, IsSuperAdminGuard]  },

    { path: 'fees', component: FeesComponent, canActivate: [IsLoggedInGuard, IsAdminGuard] },
    { path: 'fee/get/:id', component: SingleFeeComponent, canActivate: [IsLoggedInGuard, IsAdminGuard]  },
    { path: 'fee/update/:id', component: UpdateFeesComponent, canActivate: [IsLoggedInGuard, IsAdminGuard]  },
    { path: 'fee/add', component: AddFeeComponent, canActivate: [IsLoggedInGuard, IsAdminGuard]  },

    { path: 'limits', component: LimitsComponent, canActivate: [IsLoggedInGuard, IsAdminGuard] },
    { path: 'limit/get/:id', component: SingleLimitComponent, canActivate: [IsLoggedInGuard, IsAdminGuard]  },
    { path: 'limit/update/:id', component: UpdateLimitsComponent, canActivate: [IsLoggedInGuard, IsAdminGuard]  },
    { path: 'limit/add', component: AddLimitComponent, canActivate: [IsLoggedInGuard, IsAdminGuard]  },
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes)],
  providers: [IsLoggedInGuard, AuthService, IsAdminGuard, IsSuperAdminGuard,
              UserModel, AdminModel, AdminService, LimitModel, FeesModel, ProfilesService]
})
export class AppRoutingModule {}
