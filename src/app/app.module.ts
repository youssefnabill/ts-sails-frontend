import { AuthService } from './auth/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { NavBarComponent } from './static/nav-bar/nav-bar.component';
import { FooterComponent } from './static/footer/footer.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { MessagesComponent} from './messages/messages.component';
import { MessageService} from './messages/message.service';
import { UsersComponent } from './users/users.component';
import { SingleUserComponent} from './users/single-user/single-user.component';
import { FeesComponent} from './profiles/fees/fees.component';
import { LimitsComponent} from './profiles/limits/limits.component';
import { SingleFeeComponent} from './profiles/fees/single-fee/single-fee.component';
import { SingleLimitComponent} from './profiles/limits/single-limit/single-limit.component';
import { TransactionHistoryComponent} from './Transactions/transaction-history/transaction-history.component';
import { HomeComponent} from './home/home.component';
import { IsLoggedInGuard } from './guards/isLoggedIn/is-logged-in.guard';
import { IsAdminGuard } from './guards/isAdmin/is-admin.guard';
import { IsSuperAdminGuard } from './guards/isSuperAdmin/is-super-admin.guard';
import { AdminsComponent } from './admins/admins.component';
import { SingleAdminComponent } from './admins/single-admin/single-admin.component';
import { AddAdminComponent } from './admins/add-admin/add-admin.component';
import { AddFeeComponent } from './profiles/fees/add-fee/add-fee.component';
import { AddLimitComponent } from './profiles/limits/add-limit/add-limit.component';
import { UpdateFeesComponent } from './profiles/fees/update-fees/update-fees.component';
import { UpdateLimitsComponent } from './profiles/limits/update-limits/update-limits.component';
import { UpdateAdminComponent } from './admins/update-admin/update-admin.component';
import { UpdateUserComponent } from './users/update-user/update-user.component';
import { MakeTransactionComponent } from './Transactions/make-transaction/make-transaction.component';
import { SingleTransactionComponent } from './Transactions/single-transaction/single-transaction.component';
import { AllTransactionsComponent } from './Transactions/all-transactions/all-transactions.component';
import { UserProfileComponent } from './users/user-profile/user-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent,
    SigninComponent,
    SignupComponent,
    MessagesComponent,
    UsersComponent,
    SingleUserComponent,
    FeesComponent,
    LimitsComponent,
    SingleFeeComponent,
    SingleLimitComponent,
    TransactionHistoryComponent,
    HomeComponent,
    AdminsComponent,
    SingleAdminComponent,
    AddAdminComponent,
    AddFeeComponent,
    AddLimitComponent,
    UpdateFeesComponent,
    UpdateLimitsComponent,
    UpdateAdminComponent,
    UpdateUserComponent,
    MakeTransactionComponent,
    SingleTransactionComponent,
    AllTransactionsComponent,
    UserProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    MessageService,
    AuthService,
    IsLoggedInGuard,
    IsAdminGuard,
    IsSuperAdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
